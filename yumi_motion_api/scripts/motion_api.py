#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A motion_api to send command to Yumi."""

__author__ = "Maxence Grand"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

import sys
import copy
import rospy

from math import pi, tau, dist, fabs, cos, degrees
import std_msgs.msg
from geometry_msgs.msg import *
from sensor_msgs.msg import *
from moveit_msgs.msg import *
from moveit_commander import *
from moveit_commander.conversions import pose_to_list
import moveit_commander
from yumi_motion_api.srv import BlockCmd, JointStateCmd
import pandas as pd
import os, rospkg
from abb_rapid_sm_addin_msgs.srv import *
from abb_robot_msgs.srv import *
from abb_robot_msgs.msg import *
from std_msgs.msg import Header

def init_arm(
        group_name,
        planner="RRTConnect",
        pose_reference_frame="yumi_body",
        replanning = True,
        position_tolerance=0.0005,
        orientation_tolerance=0.0005,
        planning_attempts=100,
        planning_time=20,
        max_velocity=1,
        max_acceleration=1):
    """
    Init group armdfg

    @param group_name Name of the group arm
    @param planner Planner ID used
    @param pose_refeif (right):
            arm = 'right'
            group = self.group_r
        else:
            arm = 'left'
            group = self.group_lrence_frame The pose reference frame
    @param replaning True if the replanning is allowed
    @param position_tolerance Tolerance for the goal position
    @param orientation_tolerance  Tolerance for the goal orientation
    @param planning_attempts Number of planning planning_attempts
    @param planning_time Planning Ti;e
    @param max_velocity Velovity max 0<= <=1
    @param max_acceleration Acceleration max 0<= <=1
    """
    group = moveit_commander.MoveGroupCommander(group_name)
    group.set_planner_id(planner)
    group.set_pose_reference_frame(pose_reference_frame)
    group.allow_replanning(replanning)
    group.set_goal_position_tolerance(position_tolerance)
    group.set_goal_orientation_tolerance(orientation_tolerance)
    group.set_num_planning_attempts(planning_attempts)
    group.set_planning_time(planning_time)
    group.set_max_velocity_scaling_factor(max_velocity)
    group.set_max_acceleration_scaling_factor(max_acceleration)
    return group

def clamp(num, min_value, max_value):
    """Clamp"""
    return max(min(num, max_value), min_value)

class Yumi():
    """Yumi Class (Simulated)"""

    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        self.robot = RobotCommander('robot_description')
        self.group_l = init_arm("left_arm")
        self.group_r = init_arm("right_arm")
        self.group_both = init_arm("both_arms")
        display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', 	moveit_msgs.msg.DisplayTrajectory, queue_size=20)
        rospy.sleep(3)
        print(f"============ {type(self).__name__} Initialized")


    ###################################################
    ############### Status Methods ####################
    ###################################################

    def get_current_pose(self,  right=True):
        """Gets the current pose

        Return the current pose of the selected arm

        @param right True if the selected arm is the right one
        @returns Pose
        @rtype PoseStamped
        """
        return self.group_r.get_current_pose() if right \
            else self.group_l.get_current_pose()

    def get_current_rpy(self,  right=True):
        """Gets the current oint values

        Return the current position of all joints of the selected arm as Euler angles

        @param right True if the selected arm is the right one
        @returns Orientation
        @rtype Orientation
        """
        return self.group_r.get_current_rpy() if right \
            else self.group_l.get_current_rpy()

    def get_current_joint_values(self,  right=True):
        """Gets the current joint values

        Return the current joint values of the selected arm

        @param right True if the selected arm is the right one
        @returns Joint values
        @rtype float[]
        """
        return self.group_r.get_current_joint_values() if right \
            else self.group_l.get_current_joint_values()

    def print_arm_status(self, right=True):
        """prints the status of the selected arm

        @param right True if the selected arm is the right one
        @returns None
        """
        sel = "right" if right else "left"
        print(f"Yumi's {sel} arm status")
        print(self.get_current_pose(right))
        print(self.get_current_rpy(right))
        print(self.get_current_joint_values(right))

    def print_status(self):
        """Prints yumi's status

        @return None
        """
        self.print_arm_status(right=True)
        self.print_arm_status(right=False)

    def get_joints_name(self, right=True):
        if (right):
            group = self.group_r
        else:
            group = self.group_l
        return group.get_joints()

    def init_group(self, right=True):
        if (right):
            arm = 'right'
            group = self.group_r
        else:
            arm = 'left'
            group = self.group_l
        return group, arm

    ###################################################
    ############## Movements Methods ##################
    ###################################################

    def execute_plan(self, plan, right=True):
        """Plans and moves the selected arm to target

        Creates a plan to move a group to the given target.

        @param target: The pose to move to
        @param right True if the selected arm is the right one
        @returns None
        """

        group, arm = self.init_group(right=right)

        group.execute(plan, wait=True)
        group.stop()
        rospy.sleep(3)

    def go_to_pose_goal(self, target, right=True):
        """Plans and moves the selected arm to target

        Creates a plan to move a group to the given target.

        @param target: The pose to move to
        @param right True if the selected arm is the right one
        @returns None
        """
        euler = tf.transformations.euler_from_quaternion((target.orientation.x, target.orientation.y, target.orientation.z, target.orientation.w))

        group, arm = self.init_group(right=right)

        rospy.loginfo('Planning and moving '
                + arm + ' arm: Position: {'
                + str(target.position.x) + ';' + str(target.position.y) + ';'
                 + str(target.position.z) +
                '}. Rotation: {' + str(euler[0]) + ';' + str(euler[1]) + ';' + str(euler[2]) + '}.')

        group.set_pose_target(target)
        group.set_goal_position_tolerance(0.001)
        group.go(wait=True)
        group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets().
        group.clear_pose_targets()
        rospy.sleep(3)

    def go_to_joint_state(self, target, right=True):
        """Plans and moves the selected group to the joint goal

        Creates a plan to move a group to the given joint goal.

        @param target: The joint goal
        @param right True if the selected arm is the right one
        @returns None
        """

        group, arm = self.init_group(right=right)

        group.go(target,wait=True)
        # Calling `stop()` ensures that there is no residual movement
        group.stop()
        rospy.sleep(3)

    def go_to_joint_states(self, targets, right=True):
        """Plans and moves the selected group to a list of joint goals

        Creates a plan to move a group to the given joint goal.

        @param target: The joint goal
        @param right True if the selected arm is the right one
        @returns None
        """

        group, arm = self.init_group(right=right)

        for target in targets:
            group.go(target,wait=True)
        # Calling `stop()` ensures that there is no residual movement
        group.stop()
        # rospy.sleep(3)

    def move_linear(self, z_value, right=True):
        """Up the selected arm"""
        group = self.group_r if (right) else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        print(wpose.position.z)
        wpose.position.z = z_value
        print(wpose.position.z)
        waypoints = []
        waypoints.append(wpose)
        (plan, fraction) = group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold
        self.execute_plan(plan, right=right)

    def up(self, z_value, right=True):
        """Up the selected arm"""
        group = self.group_r if (right) else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        print(wpose.position.z)
        wpose.position.z = z_value
        print(wpose.position.z)
        waypoints = []
        waypoints.append(wpose)
        (plan, fraction) = group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold
        self.execute_plan(plan, right=right)

    def down(self, z_value, right=True):
        """Down the selected arm"""
        group = self.group_r if (right) else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        # print(wpose.position.z)
        wpose.position.z = z_value
        # print(wpose.position.z)
        waypoints = []
        waypoints.append(wpose)
        (plan, fraction) = group.compute_cartesian_path(
            waypoints, 0.01, 5.0  # waypoints to follow  # eef_step
        )  # jump_threshold
        print(fraction)
        self.execute_plan(plan, right=right)

    def rotate(self, x_o, y_o, z_o, w_o, right=True):
        """Rotates selected arm"""
        group = self.group_r if right else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        wpose.orientation.x = x_o
        wpose.orientation.y = y_o
        wpose.orientation.z = z_o
        wpose.orientation.w = w_o
        self.go_to_pose_goal(wpose, right=right)

    ###################################################
    ############## Movements Methods ##################
    ###################################################
    def calibrateGrippers(self):
        """calibrate grippers"""
        rospy.sleep(3)
        return

    def effort(self, effort_value, right=True):
        """gripper efforts"""
        rospy.sleep(3)
        return

class YumiEGM(Yumi):
    """Yumi Class (EGM Hardware Interface)"""

    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        self.robot = RobotCommander('robot_description')
        self.group_l = init_arm("left_arm")
        self.group_r = init_arm("right_arm")
        self.group_both = init_arm("both_arms")
        display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', 	moveit_msgs.msg.DisplayTrajectory, queue_size=20)
        rospy.sleep(3)
        print(f"============ {type(self).__name__} Initialized")
        self.sys_init()

    def sys_init(self):
        """
        Initialize the system, auto_mode, motors_on, rapid_running,
        egm_settings_adjusted
        """
        auto_mode = True # system_info.auto_mode #
        # rapid_running = system_info.rapid_running
        motors_on = False # system_info.motors_on #
        # print(auto_mode, motors_on)

        if(not auto_mode):
            print("xxxxxxxxxxxxx Robot in Manual Mode! Can't Initialize! xxxxxxxxxxxxx")
            sys.exit()
        else:
            # Reset Program Pointer to main and Start RAPID
            rospy.wait_for_service('/yumi/rws/pp_to_main')
            rospy.wait_for_service('/yumi/rws/stop_rapid')
            rospy.wait_for_service('/yumi/rws/start_rapid')
            # set_motors_off= rospy.ServiceProxy("/yumi/rws/set_motors_off",TriggerWithResultCode)
            self.stop_rapid    = rospy.ServiceProxy("/yumi/rws/stop_rapid"   , TriggerWithResultCode)
            self.pp_to_main    = rospy.ServiceProxy("/yumi/rws/pp_to_main"   , TriggerWithResultCode)
            self.start_rapid   = rospy.ServiceProxy("/yumi/rws/start_rapid"  , TriggerWithResultCode)
            # Call the functionalities
            self.stop_rapid()
            self.pp_to_main()
            time.sleep(1)
            self.start_rapid()
            # Turn Motors On
            if(not motors_on):
                rospy.wait_for_service('/yumi/rws/set_motors_on')
                self.set_motors_on = rospy.ServiceProxy("/yumi/rws/set_motors_on", TriggerWithResultCode)
                self.set_motors_on()

    def init_group(self, right=True):
        if (right):
            arm = 'right'
            group = self.group_r
            controller = "right_arm_vel_controller"
        else:
            arm = 'left'
            group = self.group_l
            controller = "left_arm_vel_controller"

        rospy.wait_for_service('/yumi/rws/sm_addin/get_egm_settings')
        rospy.wait_for_service('/yumi/rws/sm_addin/set_egm_settings')
        # rospy.wait_for_service("/yumi/rws/sm_addin/stop_egm")

        # stop_egm = rospy.ServiceProxy("/yumi/rws/sm_addin/stop_egm", TriggerWithResultCode)

        get_egm_settings = rospy.ServiceProxy("/yumi/rws/sm_addin/get_egm_settings", GetEGMSettings)
        set_egm_settings = rospy.ServiceProxy("/yumi/rws/sm_addin/set_egm_settings", SetEGMSettings)

        current_settings_L = get_egm_settings(task='T_ROB_L')
        current_settings_R = get_egm_settings(task='T_ROB_R')

        settings_L = current_settings_L.settings
        settings_R = current_settings_R.settings

        # max_speed_deviation is in deg/s, we convert from rad/s
        settings_L.activate.max_speed_deviation = degrees(7.0)
        settings_R.activate.max_speed_deviation = degrees(7.0)

        # settings.activate.cond_min_max

        # settings.run.cond_time
        settings_L.run.cond_time = 60.0
        settings_R.run.cond_time = 60.0
        # posCorrgain = 0.0
        settings_L.run.pos_corr_gain = 0.0
        settings_R.run.pos_corr_gain = 0.0

        # stop_egm()
        taskname = "T_ROB_L"
        set_egm_settings(task=taskname, settings=settings_L)

        taskname = "T_ROB_R"
        set_egm_settings(task=taskname, settings=settings_R)

        print("===================== EGM Settings Updated =====================")

        rospy.wait_for_service("/yumi/rws/sm_addin/start_egm_joint")
        rospy.wait_for_service("/yumi/rws/sm_addin/stop_egm")
        # rospy.wait_for_service("/yumi/egm/controller_manager/switch_controller")

        start_egm = rospy.ServiceProxy("/yumi/rws/sm_addin/start_egm_joint", TriggerWithResultCode)
        stop_egm = rospy.ServiceProxy("/yumi/rws/sm_addin/stop_egm", TriggerWithResultCode)

        start_egm()

        controller_conf = "start_controllers: [{}] \nstop_controllers: [''] \nstrictness: 1 \nstart_asap: false \ntimeout: 0.0".format(controller)
        import subprocess
        subprocess.run(["rosservice", "call", "/yumi/egm/controller_manager/switch_controller", controller_conf])

        return group, arm

    ###################################################
    ############## Movements Methods ##################
    ###################################################
    def calibrateGrippers(self):
        """Calibrates Yumi's Gripper"""
        rospy.wait_for_service('/yumi/rws/sm_addin/set_sg_command')
        rospy.wait_for_service('/yumi/rws/sm_addin/run_sg_routine')

        set_sg_command = rospy.ServiceProxy("/yumi/rws/sm_addin/set_sg_command", SetSGCommand)
        run_sg_routine = rospy.ServiceProxy("/yumi/rws/sm_addin/run_sg_routine", TriggerWithResultCode)

        """
        uint8 SG_COMMAND_INITIALIZE   = 3
        uint8 SG_COMMAND_CALIBRATE    = 4
        uint8 SG_COMMAND_MOVE_TO      = 5
        uint8 SG_COMMAND_GRIP_IN      = 6
        uint8 SG_COMMAND_GRIP_OUT     = 7
        """
        task_l = 'T_ROB_L'
        task_r = 'T_ROB_R'
        fin_pos = 0.0
        # # Close Grippers
        cmd = 6
        set_sg_command(task=task_l, command=cmd, target_position=fin_pos)
        set_sg_command(task=task_r, command=cmd, target_position=fin_pos)
        run_sg_routine()
        time.sleep(1)
        # # Calibrate Grippers
        cmd = 4
        set_sg_command(task=task_l, command=cmd, target_position=fin_pos)
        set_sg_command(task=task_r, command=cmd, target_position=fin_pos)
        run_sg_routine()
        time.sleep(2)
        print('===================== Grippers: Calibrated =====================')

    def effort(self, effort_value, right=True):
        """gripper efforts"""
        effort_value = clamp(effort_value, 0, 20)
        gripper_id = 'right' if right else 'left'
        rospy.loginfo("Setting gripper " + str(gripper_id) + " to " + str(effort_value))
        rospy.loginfo('Setting gripper effort to ' + str(effort_value) + ' for arm ' + str(gripper_id))

        rospy.wait_for_service('/yumi/rws/sm_addin/set_sg_command')
        rospy.wait_for_service('/yumi/rws/sm_addin/run_sg_routine')

        set_sg_command = rospy.ServiceProxy("/yumi/rws/sm_addin/set_sg_command", SetSGCommand)
        run_sg_routine = rospy.ServiceProxy("/yumi/rws/sm_addin/run_sg_routine", TriggerWithResultCode)

        task_L = 'T_ROB_L'
        task_R = 'T_ROB_R'
        cmd = 5
        if right:
            set_sg_command(task=task_R, command=cmd, target_position=effort_value)
            run_sg_routine()
            time.sleep(1)
        else:
            set_sg_command(task=task_L, command=cmd, target_position=effort_value)
            run_sg_routine()
            time.sleep(1)
        rospy.sleep(1.0)

HOME = {
    "left": [0.0, -2.2699, 2.3561, 0.5224, 0.0033, 0.6971, 0.0035],
    "right": [0.0042, -2.2704, -2.353, 0.5187, -0.0048, 0.6965, 0.0051]
}

IDLE = {
    "left": [0.0, -2.2699, 2.3561, 0.5224, 0.0033, 0.6971, 0.0035],
    "right": [0.5663643490150093, -1.6229436840997382, -1.4689153682392069, -0.062283452443413226, 0.1814169635906924, 1.1252602622270549, 0.2572715312480672]
}

def get_orientation_from_lego_orientation(horizontal=True):
    return [pi, 0, 0] if horizontal else [pi, 0, pi/2]

def create_pose_euler(x_p, y_p, z_p, roll_rad, pitch_rad, yaw_rad):
    """Creates a pose using euler angles

    Creates a pose for use with MoveIt! using XYZ coordinates and RPY
    orientation in radians

    @param x_p The X-coordinate for the pose
    @param y_p The Y-coordinate for the pose
    @param z_p The Z-coordinate for the pose
    @param roll_rad The roll angle for the pose
    @param pitch_rad The pitch angle for the pose
    @param yaw_rad The yaw angle for the pose
    @returns Pose
    @rtype PoseStamped
    """
    quaternion = tf.transformations.quaternion_from_euler(roll_rad, pitch_rad, yaw_rad)
    return create_pose(x_p, y_p, z_p, quaternion[0], quaternion[1], quaternion[2], quaternion[3])


def create_pose(x_p, y_p, z_p, x_o, y_o, z_o, w_o):
    """Creates a pose using quaternions

    Creates a pose for use with MoveIt! using XYZ coordinates and XYZW
    quaternion values

    @param x_p The X-coordinate for the pose
    @param y_p The Y-coordinate for the pose
    @param z_p The Z-coordinate for the pose
    @param x_o The X-value for the orientation
    @param y_o The Y-value for the orientation
    @param z_o The Z-value for the orientation
    @param w_o The W-value for the orientation
    @returns Pose
    @rtype PoseStamped
    """
    pose_target = geometry_msgs.msg.Pose()
    pose_target.position.x = x_p
    pose_target.position.y = y_p
    pose_target.position.z = z_p
    pose_target.orientation.x = x_o
    pose_target.orientation.y = y_o
    pose_target.orientation.z = z_o
    pose_target.orientation.w = w_o
    return pose_target


PATCH_Z_VALUE = -.1

class Block:
    """
        Block
    """

    def __init__(self, id, top_left, top_right, bottom_left, bottom_right, level=0):
        """
        Constructs a Block

        @param id: Block id
        @top_left: top left corner coordinates
        @top_right: top right corner coordinates
        @bottom_left: bottom left corner coordinates
        @bottom_right: bottom right corner coordinates
        @level: Block level
        """
        self.id = id
        self.top_left = top_left
        self.top_right = top_right
        self.bottom_left = bottom_left
        self.bottom_right = bottom_right
        self.level = level

    def is_horizontal(self):
        """
        Check if the block is horizontal
        Square block are consider as horizon

        @return True if the the block is horizontal
        """
        return self.top_right[0] - self.top_left[0] >= self.bottom_right[1] - self.top_right[1]

    def center(self):
        """Block center"""
        return (
            (self.top_right[0] + self.top_left[0])/2,
            (self.bottom_right[1] + self.top_right[1])/2
        )

    def get_mm(x):
        """Convert abstract x into mm position on the lego plate"""
        return x * 16

    def get_real_position(self, yumi):
        """
        Get the center position in the Yumi coordinates system
        """
        center = self.center()

        p = PoseStamped()
        p.header.frame_id = yumi.robot.get_planning_frame()
        p.header.stamp = rospy.Time.now()
        p.pose.position.x = 0.280 + Block.get_mm(center[1])/1000
        p.pose.position.y = -0.380 + Block.get_mm(center[0])/1000
        p.pose.position.z = 0.008*(self.level+1) + 0.055
        p.pose.orientation.x = 0.0
        p.pose.orientation.y = 0.0
        p.pose.orientation.z = 0.0
        p.pose.orientation.w = 1.0
        return p

    def get_dimension(self):
        """block dimension"""
        return [
            Block.get_mm(self.bottom_right[1] - self.top_right[1])/1000,
            Block.get_mm(self.top_right[0] - self.top_left[0])/1000,
            0.016*(1+self.level)
        ]

def get_joints_plan(req, pick=True):
    """Return subtasks plan"""
    rospack = rospkg.RosPack()
    arm = "right" if req.right else "left"
    path = rospack.get_path("yumi_motion_api")
    plan = {}
    if(pick):
        csvfile = f"{path}/joint_states/pick_{arm}"
        csvfile = f"{csvfile}_{req.block_id}.csv"
        df = pd.DataFrame(pd.read_csv(csvfile))
        plan = {
        "above" : [df.loc[df['state_name'] == 'above'][f"joint{i}"].values[0] for i in range(1,8)],
        "block" : [df.loc[df['state_name'] == 'block'][f"joint{i}"].values[0] for i in range(1,8)]
        }
    else:
        csvfile = f"{path}/joint_states/place_{arm}"
        csvfile = f"{csvfile}_{req.block_id}.csv"
        df = pd.DataFrame(pd.read_csv(csvfile))
        plan = {
        "above" : [df.loc[df['state_name'] == 'above'][f"joint{i}"].values[0] for i in range(1,8)],
        "block" : [df.loc[df['state_name'] == 'block'][f"joint{i}"].values[0] for i in range(1,8)]
        }
    return plan, req.right

class MotionAPI(object):
    """MotionAPI"""

    def __init__(self, simulation=True):
        super(MotionAPI, self).__init__()
        rospy.init_node("motion_api", anonymous=True)
        print("============ Node motion_api Initialized")
        rospy.sleep(2)
        """Init motion interface"""
        self.scene = PlanningSceneInterface()

        if(simulation):
            self.yumi = Yumi()
        else:
            self.yumi = YumiEGM()
        self.add_scene_table()
        self.calibrateGrippers()


    def print_status(self):
        """Print Status"""
        self.yumi.print_status()

    def add_scene_table(self):
        """Add a table on the scene"""
        self.scene.remove_world_object("table")
        p = PoseStamped()
        p.header.frame_id = self.yumi.robot.get_planning_frame()
        p.header.stamp = rospy.Time.now()
        p.pose.position.x = 0.405
        p.pose.position.y = 0.000
        p.pose.position.z = 0.050 # Table Height
        p.pose.orientation.x = 0.0
        p.pose.orientation.y = 0.0
        p.pose.orientation.z = 0.0
        p.pose.orientation.w = 1.0
        self.scene.add_box("table", p, (0.510, 1.000, 0.001))

    ###################################################
    ############## Movements Methods ##################
    ###################################################

    def go_to(self, x_p, y_p, z_p, roll_rad, pitch_rad, yaw_rad, right=True, status=False):
        """Set end effector position

        Sends a command to MoveIt! to move to the selected position, in any way
        it sees fit.

        @param x_p: The X-coordinate for the pose
        @param y_p: The Y-coordinate for the pose
        @param z_p: The Z-coordinate for the pose
        @param roll_rad: The roll angle for the pose
        @param pitch_rad: The pitch angle for the pose
        @param yaw_rad: The yaw angle for the pose
        @param right True if the selected arm is the right one
        @returns None
        """
        z_p += PATCH_Z_VALUE
        target = create_pose_euler(x_p, y_p, z_p, roll_rad, pitch_rad, yaw_rad)
        self.yumi.go_to_pose_goal(target, right=right)
        if(status):
            self.print_status()

    def go_to_home(self):
        """ Moves yumi to its calib position"""
        self.yumi.go_to_joint_state(HOME["left"], right=False)
        self.yumi.go_to_joint_state(HOME["right"], right=True)
        self.print_status()

    def go_to_idle(self):
        """ Moves yumi to its calib position"""
        self.yumi.go_to_joint_state(IDLE["left"], right=False)
        self.yumi.go_to_joint_state(IDLE["right"], right=True)
        self.print_status()

    def rotate(self, roll_rad, pitch_rad, yaw_rad, right=True):
        """rotates the selected arm"""
        x_o, y_o, z_o, w_o = tf.transformations.quaternion_from_euler(roll_rad, pitch_rad, yaw_rad)
        self.yumi.rotate(x_o, y_o, z_o, w_o, right=right)

    def down(self, z_value, right=True):
        """Down arm"""
        self.yumi.move_linear(z_value + PATCH_Z_VALUE, right=right)

    def up(self, z_value, right=True):
        """Down arm"""
        self.yumi.move_linear(z_value + PATCH_Z_VALUE, right=right)

    def move_linear(self, z_value, right=True):
        """Down arm"""
        self.yumi.move_linear(z_value + PATCH_Z_VALUE, right=right)

    ###################################################
    ############### Grippers Methods ##################
    ###################################################

    def calibrateGrippers(self):
        """Calibrates Yumi's grippers"""
        self.yumi.calibrateGrippers()

    def close_gripper(self, right=True):
        """Close gripper

        Close the selected gripper.

        @param right True if the selected arm is the right one
        @returns None
        """
        self.yumi.effort(0,right=right)

    def open_gripper(self, right=True):
        """Open gripper

        Open the selected gripper.

        @param right True if the selected arm is the right one
        @returns None
        """
        self.yumi.effort(20,right=right)

    def grasp(self, right=True):
        """close gripper to grasp

        Open the selected gripper.

        @param right True if the selected arm is the right one
        @returns None
        """
        self.yumi.effort(15,right=right)

    ###################################################
    ############### Services Methods ##################
    ###################################################

    def pick(self, req):
        """Pick the block"""
        rospy.loginfo(f'New Service call: Pick Block {req.block_id}')

        plan, right = get_joints_plan(req, pick=True)
        rospy.loginfo(f'Pick Block {req.block_id}: Step 1: Open the gripper ')
        self.open_gripper(right=right)

        rospy.loginfo(f'Pick Block {req.block_id}: Step 2: Move to pick the block')
        self.yumi.go_to_joint_states([plan["above"], plan["block"]], right=right)
        # self.yumi.go_to_joint_state(plan["above"], right=right)
        # self.yumi.go_to_joint_state(plan["block"], right=right)

        rospy.loginfo(f'Pick Block {req.block_id}: Step 3: Grasp the block')
        self.grasp(right=right)

        rospy.loginfo(f'Pick Block {req.block_id}: Step 4: Pick the block')
        self.yumi.go_to_joint_states([plan["above"]], right=right)
        return 0


    def place(self, req):
        """Place the block"""
        rospy.loginfo(f'New Service call: Place Block {req.block_id}')

        plan, right = get_joints_plan(req, pick=False)
        rospy.loginfo(f'Place Block {req.block_id}: Step 1: Move the gripper to the new block position')
        self.yumi.go_to_joint_states([plan["above"], plan["block"]], right=right)

        rospy.loginfo(f'Place Block {req.block_id}: Step 2: Release the block')
        self.open_gripper(right=right)

        rospy.loginfo(f'Place Block {req.block_id}: Step 3: Move to home')
        arm = "right" if right else "left"
        self.yumi.go_to_joint_states([plan["above"], HOME[arm]], right=right)

        rospy.loginfo(f'Place Block {req.block_id}: Step 4: Close the Gripper')
        self.close_gripper(right=right)

        return 0

    def joints(self, req):
        """Get joint states"""
        right = req.right_arm
        joints_ = JointState()
        joints_.header = Header()
        joints_.header.stamp = rospy.Time.now()
        joints_.name = self.yumi.get_joints_name(right=right)
        joints_.position = self.yumi.get_current_joint_values(right)
        joints_.velocity = []
        joints_.effort = []
        return joints_

def main(simulation):
    try:
        api = MotionAPI(simulation=simulation)

        serv_pick = rospy.Service('/motion_api/pick', BlockCmd, api.pick)
        rospy.loginfo("Service /motion_api/pick ready")
        serv_place = rospy.Service('/motion_api/place', BlockCmd, api.place)
        rospy.loginfo("Service /motion_api/place ready")
        serv_place = rospy.Service('/motion_api/joints', JointStateCmd, api.joints)
        rospy.loginfo("Service /motion_api/joints ready")
        rospy.spin()

        # blue_next = Block(
        #     0,
        #     [24,12],
        #     [25,12],
        #     [25,13],
        #     [24,13]
        # )
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

import argparse

if __name__ == "__main__":
    main(True)
