#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

""" A moveit interface to control Yumi."""

__author__ = "Belal Hmedan"
__copyright__ = "TODO"
__credits__ = ["TODO"]
__license__ = "TODO"
__version__ = "0.0.1"
__maintainer__ = "Maxence Grand"
__email__ = "Maxence.Grand@univ-grenoble-alpes.fr"
__status__ = "Under Developpement"

def init_arm(
        group_name,
        planner="RRTConnect",
        pose_reference_frame="yumi_body",
        replanning = True,
        position_tolerance=0.0005,
        orientation_tolerance=0.0005,
        planning_attempts=100,
        planning_time=20,
        max_velocity=1,
        max_acceleration=1):
    """
    Init group armdfg

    @param group_name Name of the group arm
    @param planner Planner ID used
    @param pose_refeif (right):
            arm = 'right'
            group = self.group_r
        else:
            arm = 'left'
            group = self.group_lrence_frame The pose reference frame
    @param replaning True if the replanning is allowed
    @param position_tolerance Tolerance for the goal position
    @param orientation_tolerance  Tolerance for the goal orientation
    @param planning_attempts Number of planning planning_attempts
    @param planning_time Planning Ti;e
    @param max_velocity Velovity max 0<= <=1
    @param max_acceleration Acceleration max 0<= <=1
    """
    group = moveit_commander.MoveGroupCommander(group_name)
    print(group.get_joints())
    group.set_planner_id(planner)
    group.set_pose_reference_frame(pose_reference_frame)
    group.allow_replanning(replanning)
    group.set_goal_position_tolerance(position_tolerance)
    group.set_goal_orientation_tolerance(orientation_tolerance)
    group.set_num_planning_attempts(planning_attempts)
    group.set_planning_time(planning_time)
    group.set_max_velocity_scaling_factor(max_velocity)
    group.set_max_acceleration_scaling_factor(max_acceleration)
    return group
    
class Yumi():
    """Yumi Class (Simulated)"""

    def __init__(self):
        moveit_commander.roscpp_initialize(sys.argv)
        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        self.robot = RobotCommander('robot_description')
        self.group_l = init_arm("left_arm")
        self.group_r = init_arm("right_arm")
        self.group_both = init_arm("both_arms")
        display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', 	moveit_msgs.msg.DisplayTrajectory, queue_size=20)
        rospy.sleep(3)
        print("============ Yumi Initialized")


    ###################################################
    ############### Status Methods ####################
    ###################################################

    def get_current_pose(self,  right=True):
        """Gets the current pose

        Return the current pose of the selected arm

        @param right True if the selected arm is the right one
        @returns Pose
        @rtype PoseStamped
        """
        return self.group_r.get_current_pose() if right \
            else self.group_l.get_current_pose()

    def get_current_rpy(self,  right=True):
        """Gets the current oint values

        Return the current position of all joints of the selected arm as Euler angles

        @param right True if the selected arm is the right one
        @returns Orientation
        @rtype Orientation
        """
        return self.group_r.get_current_rpy() if right \
            else self.group_l.get_current_rpy()

    def get_current_joint_values(self,  right=True):
        """Gets the current joint values

        Return the current joint values of the selected arm

        @param right True if the selected arm is the right one
        @returns Joint values
        @rtype float[]
        """
        return self.group_r.get_current_joint_values() if right \
            else self.group_l.get_current_joint_values()

    def print_arm_status(self, right=True):
        """prints the status of the selected arm

        @param right True if the selected arm is the right one
        @returns None
        """
        sel = "right" if right else "left"
        print(f"Yumi's {sel} arm status")
        print(self.get_current_pose(right))
        print(self.get_current_rpy(right))
        print(self.get_current_joint_values(right))

    def print_status(self):
        """Prints yumi's status

        @return None
        """
        self.print_arm_status(right=True)
        self.print_arm_status(right=False)


    def init_group(self, right=True):
        if (right):
            arm = 'right'
            group = self.group_r
        else:
            arm = 'left'
            group = self.group_l
        return group, arm

    ###################################################
    ############## Movements Methods ##################
    ###################################################

    def execute_plan(self, plan, right=True):
        """Plans and moves the selected arm to target

        Creates a plan to move a group to the given target.

        @param target: The pose to move to
        @param right True if the selected arm is the right one
        @returns None
        """

        group, arm = self.init_group(right=right)

        group.execute(plan, wait=True)
        group.stop()
        rospy.sleep(3)

    def go_to_pose_goal(self, target, right=True):
        """Plans and moves the selected arm to target

        Creates a plan to move a group to the given target.

        @param target: The pose to move to
        @param right True if the selected arm is the right one
        @returns None
        """
        euler = tf.transformations.euler_from_quaternion((target.orientation.x, target.orientation.y, target.orientation.z, target.orientation.w))

        group, arm = self.init_group(right=right)

        rospy.loginfo('Planning and moving '
                + arm + ' arm: Position: {'
                + str(target.position.x) + ';' + str(target.position.y) + ';'
                 + str(target.position.z) +
                '}. Rotation: {' + str(euler[0]) + ';' + str(euler[1]) + ';' + str(euler[2]) + '}.')

        group.set_pose_target(target)
        group.set_goal_position_tolerance(0.001)
        group.go(wait=True)
        group.stop()
        # It is always good to clear your targets after planning with poses.
        # Note: there is no equivalent function for clear_joint_value_targets().
        group.clear_pose_targets()
        rospy.sleep(3)

    def go_to_joint_state(self, target, right=True):
        """Plans and moves the selected group to the joint goal

        Creates a plan to move a group to the given joint goal.

        @param target: The joint goal
        @param right True if the selected arm is the right one
        @returns None
        """

        group, arm = self.init_group(right=right)

        group.go(target,wait=True)
        # Calling `stop()` ensures that there is no residual movement
        group.stop()
        rospy.sleep(3)

    def go_to_joint_states(self, targets, right=True):
        """Plans and moves the selected group to a list of joint goals

        Creates a plan to move a group to the given joint goal.

        @param target: The joint goal
        @param right True if the selected arm is the right one
        @returns None
        """

        group, arm = self.init_group(right=right)

        for target in targets:
            group.go(target,wait=True)
        # Calling `stop()` ensures that there is no residual movement
        group.stop()
        rospy.sleep(3)

    def move_linear(self, z_value, right=True):
        """Up the selected arm"""
        group = self.group_r if (right) else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        print(wpose.position.z)
        wpose.position.z = z_value
        print(wpose.position.z)
        waypoints = []
        waypoints.append(wpose)
        (plan, fraction) = group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold
        self.execute_plan(plan, right=right)

    def up(self, z_value, right=True):
        """Up the selected arm"""
        group = self.group_r if (right) else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        print(wpose.position.z)
        wpose.position.z = z_value
        print(wpose.position.z)
        waypoints = []
        waypoints.append(wpose)
        (plan, fraction) = group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
        )  # jump_threshold
        self.execute_plan(plan, right=right)

    def down(self, z_value, right=True):
        """Down the selected arm"""
        group = self.group_r if (right) else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        # print(wpose.position.z)
        wpose.position.z = z_value
        # print(wpose.position.z)
        waypoints = []
        waypoints.append(wpose)
        (plan, fraction) = group.compute_cartesian_path(
            waypoints, 0.01, 5.0  # waypoints to follow  # eef_step
        )  # jump_threshold
        print(fraction)
        self.execute_plan(plan, right=right)

    def rotate(self, x_o, y_o, z_o, w_o, right=True):
        """Rotates selected arm"""
        group = self.group_r if right else self.group_l
        wpose = copy.deepcopy(group.get_current_pose()).pose
        wpose.orientation.x = x_o
        wpose.orientation.y = y_o
        wpose.orientation.z = z_o
        wpose.orientation.w = w_o
        self.go_to_pose_goal(wpose, right=right)

    ###################################################
    ############## Movements Methods ##################
    ###################################################
    def calibrateGrippers(self):
        """calibrate grippers"""
        return

    def effort(self, effort_value, right=True):
        """gripper efforts"""
        return

HOME = {
    "left": [0.0, -2.2699, 2.3561, 0.5224, 0.0033, 0.6971, 0.0035],
    "right": [0.0042, -2.2704, -2.353, 0.5187, -0.0048, 0.6965, 0.0051]
    # "right":[1.1015723699824334, -1.7478509832404054, -1.481914219561203, 0.18485387391245572, 0.7643431899827013, -0.8809056632052379, 0.09463194696309844]
}

def get_orientation_from_lego_orientation(horizontal=True):
    return [pi, 0, 0] if horizontal else [pi, 0, pi/2]

def create_pose_euler(x_p, y_p, z_p, roll_rad, pitch_rad, yaw_rad):
    """Creates a pose using euler angles

    Creates a pose for use with MoveIt! using XYZ coordinates and RPY
    orientation in radians

    @param x_p The X-coordinate for the pose
    @param y_p The Y-coordinate for the pose
    @param z_p The Z-coordinate for the pose
    @param roll_rad The roll angle for the pose
    @param pitch_rad The pitch angle for the pose
    @param yaw_rad The yaw angle for the pose
    @returns Pose
    @rtype PoseStamped
    """
    quaternion = tf.transformations.quaternion_from_euler(roll_rad, pitch_rad, yaw_rad)
    return create_pose(x_p, y_p, z_p, quaternion[0], quaternion[1], quaternion[2], quaternion[3])


def create_pose(x_p, y_p, z_p, x_o, y_o, z_o, w_o):
    """Creates a pose using quaternions

    Creates a pose for use with MoveIt! using XYZ coordinates and XYZW
    quaternion values

    @param x_p The X-coordinate for the pose
    @param y_p The Y-coordinate for the pose
    @param z_p The Z-coordinate for the pose
    @param x_o The X-value for the orientation
    @param y_o The Y-value for the orientation
    @param z_o The Z-value for the orientation
    @param w_o The W-value for the orientation
    @returns Pose
    @rtype PoseStamped
    """
    pose_target = geometry_msgs.msg.Pose()
    pose_target.position.x = x_p
    pose_target.position.y = y_p
    pose_target.position.z = z_p
    pose_target.orientation.x = x_o
    pose_target.orientation.y = y_o
    pose_target.orientation.z = z_o
    pose_target.orientation.w = w_o
    return pose_target
