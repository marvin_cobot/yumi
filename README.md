# Yumi ROS Interface
ABB YuMi IRB 14000 ROS Architeture

> This repo is a fork of this [yumi ros interface repo](https://github.com/bhomaidan1990/yumi_ros) dealing with the ROS Noetic distribution

This depot is currently being developed. Important issues are still on the TODO list. Using this module to control a real Yumi robot could cause damages.
It is therefore preferable to use this module in a simulation and not on a real robot.
The authors are not responsible for any damage caused to a real robot as a result of using this module.

## Installation

All the dependencies and the whole procedure to install the Yumi ROS interface are detailed in [the install file](INSTALL.md).

## Controlling Yumi


Yumi can be controlled using [services](SERVICES.md) in two different ways:

* In a [simulation](docs/moveit_in_simulation.md)
* In [live](docs/moveit_in_live.md)
