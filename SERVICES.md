# Yumi Services

## Motion API

motion_api serves as a high-level interface designed to facilitate block manipulation with Yumi without requiring the user to directly interact with MoveIt. The primary goal of motion_api is to abstract away the complexities of motion planning, providing users with a simplified means of controlling Yumi's actions.

By functioning as a black box for motion planning, motion_api allows users to focus on specifying manipulation actions rather than dealing with the intricacies of motion planning algorithms. With motion_api, users can intuitively command Yumi to perform tasks such as grasping, moving, and releasing blocks without the need to manage low-level motion planning details.

motion_api offers two key services, `/motion_api/pick` and `/motion_api/place`, designed to facilitate the picking up and placing of blocks with Yumi. These services abstract the complexities of motion planning, providing users with a simplified means of controlling Yumi's actions when interacting with blocks.

### BlockCmd Service Message

The `BlockCmd` service message is used to command Yumi's manipulation actions related to blocks. It encapsulates the necessary information to specify the block to manipulate and its associated parameters.

#### Request

 The request message contains the following fields:

    block_id (int16): An identifier for the block to manipulate.
    top_left (int16[2]): An array representing the coordinates of the top right corner of the block to manipulate.
    top_right (int16[2]): An array representing the coordinates of the top left corner of the block to manipulate.
    bottom_left (int16[2]): An array representing the coordinates of the bottom left corner of the block to manipulate.
    bottom_right (int16[2]): An array representing the coordinates of the bottom right corner of the block to manipulate.
    right (bool): A boolean indicating if the robot has to use the right arm

#### Response

The response message contains the following field:

    code (int32): An integer representing the code result of the command execution. It returns 0 if there is no error, otherwise, it returns a non-zero error code. Currently, in case of an error, the value 1 is returned. The handling of multiple error codes will be implemented in future versions.

### Use Case

#### Pick and Place a block

We'll see how to use services A and B to take and place a block from position X to position Y.

In [terminal 1], launch the (simulated or real) robot Yumi. For more details please see the [readme](README.md)

In [Terminal 2], launch the node motion_api

    rosrun yumi_motion_api motion_api

In [terminal 3], use the following command to pick the block

    rosservice call /motion_api/pick "block_id: 0
    top_left: [4, 4]
    top_right: [4, 5]
    bottom_left: [5, 4]
    bottom_right: [5, 5]
    right: true
    "


In [terminal 3], use the following command to place the block

    rosservice call /motion_api/place "block_id: 0
    top_left: [4, 4]
    top_right: [4, 5]
    bottom_left: [5, 4]
    bottom_right: [5, 5]
    right: true
    "
